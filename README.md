## Bienvenido a esta prueba técnica de VUE JS

**Para la siguiente pruebas contarás con máximo 4 horas**

La prueba consiste en realizar una implementación desde cero de una página que navegue la serie **Rick and Morty**. 
Se deben contar con las siguientes directivas:

1. Un landing donde se liste todos los personajes en cards con su respetiva imagen y nombre. Debe tener un formato de 4 columnas. 
2. Un buscador de personajes (la funcionalidad de búsqueda puede manejarse desde front o puedes usar el siguiente endpoint https://rickandmortyapi.com/documentation/#filter-characters). 
3. Al oprimir una card debe abrir el detalle del personaje en una página nueva
4. La aplicación debe ser responsive.

Se deben utilizar las tecnologías:
1. Axios
2. Vuex
3. Vue Roouter
4. Element https://element.eleme.io
5. Typescript, vue-property-decorator, vuex-class
6. Sass
7. Jest (opcional)

El API Rest a usar es el siguiente: https://rickandmortyapi.com/documentation/#rest 

Debes iniciar un proyecto desde cero, utiliza la organización de carpetas y arquitectura que más te guste.

 